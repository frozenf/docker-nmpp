## Docker (Nginx, PHP, MySQL, PHPmyAdmin)

### Run Server
```
docker compose up -d
```

### Command NMPP
- Run Artisan
```
./nmpp <dir> <php74/php81> artisan <command> 
```
Example Run Migration in Directory apps/test using php 7.4 :
```
./nmpp test php74 artisan migrate 
```
- Run Composer
```
./nmpp <dir> <php74/php81> composer <command> 
```
Example Run Composer Install in Directory apps/test using php 7.4 :
```
./nmpp test php74 composer install 
```
- Run Node
```
./nmpp <dir> <php74/php81> npm <command> 
```
Example Run NPM Install in Directory apps/test using php 7.4 :
```
./nmpp test php74 npm install 
```